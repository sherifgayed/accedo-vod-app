import React from 'react';
import _ from 'lodash';
import {
  Container,
  Row,
  Col,
  Modal,
  ModalBody
} from 'reactstrap';
import styled from 'styled-components';

const ModalStyled = styled(Modal)`
  display: flex;
  justify-content: center;
  align-items: center;
  
  .modal-content,
  .modal-body {
    width: 675px;
    height: 394px;
  }
`;

/**
 * Carousel component,
 * this is used to display live and previously watched videos.
 */
class Carousel extends React.Component {

  /**
   * Initialise Carousel state.
   *
   * @params props object
   */
  constructor(props) {
    super(props);
    this.state = {
      videoUrl: null,
      isVideoOpen: false,
    };
  }

  /**
   * Open the video modal and add video to history.
   *
   * @param video object
   * @param event object
   */
  openVideo(video, event) {
    event.preventDefault();

    this.setState({
      videoUrl: _.get(video, 'contents.0.url', ''),
      isVideoOpen: true,
    });

    // Only add live videos to history,
    // but not previously watched videos.
    if (this.props.saveHistory) {
      const history = localStorage.getItem('history');
      let newHistory = history ? JSON.parse(history) : [];
      newHistory = newHistory.filter(vid => vid.id !== video.id);
      newHistory.push(video);
      localStorage.setItem('history', JSON.stringify(newHistory));
    }
  }

  /**
   * Close video modal.
   */
  toggle() {
    this.setState({
      videoUrl: null,
      isVideoOpen: false,
    });
  }

  /**
   * Render carousel.
   *
   * @return object
   */
  render() {
    return (
      <div>
        <ModalStyled
          isOpen={this.state.isVideoOpen}
          toggle={() => this.toggle()}
          autoFocus={true}
          centered
        >
          <ModalBody>
            <video
              src={this.state.videoUrl}
              controls
              autoPlay
              onEnded={() => this.toggle()}
            >
              Your browser doesn{'\''}t support HTML5 video.
            </video>
          </ModalBody>
        </ModalStyled>
        <Container>
          <Row>
            {this.props.data.map(video => (
              <Col xs="12" sm="6" md="4" lg="3" key={video.id}>
                <a href={_.get(video, 'contents.0.url', '')} onClick={
                  event => this.openVideo(video, event)
                }>
                  <img
                    src={_.get(video, 'images.0.url', '')}
                    width="90%"
                    height="auto"
                    id={_.get(video, 'images.0.id', '')}
                    alt={_.get(video, 'title', '')}
                  />
                  <p>{_.get(video, 'title', '')}</p>
                </a>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    );
  }
};

export default Carousel;
