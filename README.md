## Build/Install instructions

Please install nvm and run the following command from inside the projects root directory...

`nvm install && npm install && npm run start`

Browse to http://localhost:3000/

## Implemented features

1. Load from API and display a list of videos in a scrollable horizontal carousel
on the home page. Each tile displays a movie title and an associated
cover image.

2. User can select a video and play it back in full screen. When
playback is finished or user quits it, user is taken back to home page.

3. “Previously watched” carousel on the home page. It is
updated and re-sorted according to the most recently watched video.

4. The user can use a mouse and keyboard (Tab/Enter keys)
to select the video.

5. Layout size adjustment. The application can adjust layout
proportionally based on the desktop browser width.

6. Responsive design. Change carousel to Portrait view grid if application is
run on mobile device.

7. Persistent storage of watched items.

8. Content list refresh button.

## Not implemented features

1. Image caching.

2. Error handling.

3. Unit tests.

## Known bugs

1. Arrow keys do not select between videos.

2. If video data is missing a broken tile is displayed.
