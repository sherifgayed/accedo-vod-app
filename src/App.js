import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import axios from 'axios';
import _ from 'lodash';
import styled from 'styled-components';
import Config from './Config';
import Carousel from './Carousel';

const ContainerStyled = styled.div`
  background-color: #ebebeb;

  .row {
    width: 100%;
    display: flex;
    justify-content: center;

    div {
      margin: 4px;
      border: 1px solid #dddddd;
      text-align: center;

      :hover {
        border: 1px solid #3c7fc7;
      }

      img {
        margin: 16px 8px;
      }

      a {
        text-decoration: none;
        color: #888888;
      }

      a:hover {
        color: #3c7fc7;
      }
    }
  }
`;

const Header = styled.div`
  width: 100%;
  height: 20%;
  padding: 20px;
  background-color: #808080;
  color: #ffffff;
  display: flex;
  justify-content: space-between;
`;

/**
 * Main React component.
 */
class App extends React.Component {

  /**
   * Initialise App state.
   *
   * @params props object
   */
  constructor(props) {
    super(props);
    this.state = {
      live: [],
      history: [],
    };
  }

  /**
   * Load data from API.
   */
  async loadData() {
    const videos = await axios(Config.API_URL);

    this.setState({
      live: _.get(videos, 'data.entries', []),
    });
  }

  /**
   * Load history from local storage every two seconds.
   */
  fetchHistory() {
    setInterval(() => {
      const history = localStorage.getItem('history')
        ? JSON.parse(localStorage.getItem('history'))
        : [];
      history.reverse();
      this.setState({
        history,
      });
    }, 2000);
  }

  /**
   * Load data and start history fetching,
   * once the component mounts.
   */
  async componentDidMount() {
    await this.loadData();
    this.fetchHistory();
  }

  /**
   * Render the main component.
   *
   * @return object
   *
   */
  render() {
    return (
      <ContainerStyled>
        <Header>
          <div>Home</div>
          <div>
            <button onClick={async () => await this.loadData()}>
              Refresh list
            </button>
          </div>
        </Header>
        <Carousel data={this.state.live} saveHistory />
        <Header><div>Previously Watched</div></Header>
        <Carousel data={this.state.history} />
      </ContainerStyled>
    );
  }
};

export default App;
